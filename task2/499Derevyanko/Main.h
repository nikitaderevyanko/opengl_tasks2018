#pragma once

#include "common/Application.hpp"
#include "common/Mesh.hpp"
#include "common/ShaderProgram.hpp"
#include "common/Texture.hpp"
#include "common/LightInfo.hpp"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <string>
#include <map>
#include <vector>
#include <iostream>


glm::vec3 computeNormal(
    glm::vec3 const& v1,
    glm::vec3 const& v2,
    glm::vec3 const& v3
)
{
    return glm::normalize(glm::cross(v2 - v1, v3 - v1));
}


void printMat3(glm::mat3& mat);


bool isEqual(float x, float y, float eps);


glm::mat3 rotateMatrix(
    float alphaCos, float alphaSin,
    float betaCos, float betaSin,
    float gammaCos, float gammaSin
)
{
    glm::mat3 rotateX(1.0f);
    // columns !!!
    rotateX[1] = glm::vec3(0.0f, alphaCos, alphaSin);
    rotateX[2] = glm::vec3(0.0f, -alphaSin, alphaCos);

    glm::mat3 rotateY(1.0f);
    // columns !!!
    rotateY[0] = glm::vec3(betaCos, 0.0f, -betaSin);
    rotateY[2] = glm::vec3(betaSin, 0.0f, betaCos);

    glm::mat3 rotateZ(1.0f);
    // columns !!!
    rotateZ[0] = glm::vec3(gammaCos, gammaSin, 0.0f);
    rotateZ[1] = glm::vec3(-gammaSin, gammaCos, 0.0f);

    glm::mat3 result = rotateX * rotateY * rotateZ;

    return result;
}


glm::mat3 rotateMatrix(
    float alpha = 0.0f,
    float beta = 0.0f,
    float gamma = 0.0f
)
{
    if (alpha == 0.0f && beta == 0.0f && gamma == 0.0f) {
        return glm::mat3(1.0f);
    }

    return rotateMatrix(cos(alpha), sin(alpha),
                        cos(beta), sin(beta),
                        cos(gamma), sin(gamma));
}


glm::mat3 rotateMatrix(glm::vec3 angles)
{
    float alpha = angles.x;
    float beta = angles.y;
    float gamma = angles.z;

    return rotateMatrix(alpha, beta, gamma);
}


void add_plate(std::vector<glm::vec3>& vertices,
               std::vector<glm::vec3>& normals,
               std::vector<glm::vec2>& texcoords,
               glm::vec3& v1, glm::vec3& v2, glm::vec3& v3, glm::vec3& v4,
               glm::vec2& t1, glm::vec2& t2, glm::vec2& t3, glm::vec2& t4)
{
    glm::vec3 normal = computeNormal(v1, v2, v3);
    vertices.push_back(v1);
    texcoords.push_back(t1);
    vertices.push_back(v2);
    texcoords.push_back(t2);
    vertices.push_back(v3);
    texcoords.push_back(t3);
    vertices.push_back(v3);
    texcoords.push_back(t3);
    vertices.push_back(v2);
    texcoords.push_back(t2);
    vertices.push_back(v4);
    texcoords.push_back(t4);

    for (size_t i = 0; i < 6; ++i) {
        normals.push_back(normal);
    }
}

MeshPtr makeLeave(float size, bool faceOrientation = true) {
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texcoords;

    float width = size * 0.5f * 7.5f;
    float height = size * 1.3f * 15.0f;

    glm::vec3 vertex1 = glm::vec3(0.0f, 0.0f, 0.0f);
    glm::vec3 vertex2 = glm::vec3(0.0, -width, height * 0.5f);
    glm::vec3 vertex3 = glm::vec3(0.0f, width, height * 0.5f);
    glm::vec3 vertex4 = glm::vec3(0.0f, 0.0f, height);

    glm::vec2 texcoord1 = glm::vec2(0.0f, 0.5f);
    glm::vec2 texcoord2 = glm::vec2(0.5f, 0.0f);
    glm::vec2 texcoord3 = glm::vec2(0.5f, 1.0f);
    glm::vec2 texcoord4 = glm::vec2(1.0f, 0.5f);

    if (faceOrientation) {
        add_plate(vertices, normals, texcoords,
                  vertex1, vertex2, vertex3, vertex4,
                  texcoord1, texcoord2, texcoord3, texcoord4);
    } else {
        add_plate(vertices, normals, texcoords,
                  vertex1, vertex3, vertex2, vertex4,
                  texcoord1, texcoord3, texcoord2, texcoord4);
    }
    DataBufferPtr vertexBuf = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    vertexBuf->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr colorBuf = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    colorBuf->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr coordBuf = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    coordBuf->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

    MeshPtr mesh = std::make_shared<Mesh>();

    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, vertexBuf);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, colorBuf);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, coordBuf);

    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    return mesh;
}

MeshPtr makeCylinder(float r1, float r2, float height, size_t N, glm::mat3& rotateM) {
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texcoords;

    float delta = 2.0f * (float)glm::pi<float>() / (float)N;
    float phi = 0.0;

    for (size_t i = 0; i < N; ++i) {
        phi = delta * float(i);

        glm::vec3 vertex1 = glm::vec3(r1 * cos(phi), r1 * sin(phi), 0.0f);
        vertex1 = rotateM * vertex1;
        glm::vec3 vertex2 = glm::vec3(r1 * cos(phi + delta), r1 * sin(phi + delta), 0.0f);
        vertex2 = rotateM * vertex2;
        glm::vec3 vertex3 = glm::vec3(r2 * cos(phi), r2 * sin(phi), height);
        vertex3 = rotateM * vertex3;
        glm::vec3 vertex4 = glm::vec3(r2 * cos(phi + delta), r2 * sin(phi + delta), height);
        vertex4 = rotateM * vertex4;

        float width = glm::distance(vertex1, vertex2);
        float texCoordWidth = width / height;

        glm::vec2 texcoord1 = glm::vec2(0.0f, 0.0f);
        glm::vec2 texcoord2 = glm::vec2(texCoordWidth, 0.0f);
        glm::vec2 texcoord3 = glm::vec2(0.0f, 1.0f);
        glm::vec2 texcoord4 = glm::vec2(texCoordWidth, 1.0f);

        add_plate(vertices, normals, texcoords,
                  vertex1, vertex2, vertex3, vertex4,
                  texcoord1, texcoord2, texcoord3, texcoord4);
    }

    DataBufferPtr vertexBuf = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    vertexBuf->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr colorBuf = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    colorBuf->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr coordBuf = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    coordBuf->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

    MeshPtr mesh = std::make_shared<Mesh>();

    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, vertexBuf);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, colorBuf);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, coordBuf);

    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    return mesh;
}


MeshPtr makeCylinder(float r1, float r2, float height, size_t N,
                     float alpha=0.0, float beta=0.0, float gamma=0.0) {
    glm::mat3 rotateM = rotateMatrix(alpha, beta, gamma);

    return makeCylinder(r1, r2, height, N, rotateM);
}
