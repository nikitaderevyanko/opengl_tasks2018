#include "Main.h"

#include <stack>


void printMat3(glm::mat3& mat) {
    std::cout << "x: " << mat[0].x << " "
              << "x: " << mat[1].x << " "
              << "x: " << mat[2].x << " "
              << std::endl;
    std::cout << "y: " << mat[0].y << " "
              << "y: " << mat[1].y << " "
              << "y: " << mat[2].y << " "
              << std::endl;
    std::cout << "z: " << mat[0].z << " "
              << "z: " << mat[1].z << " "
              << "z: " << mat[2].z << std::endl;
}


bool isEqual(float x, float y, float eps = 0.000001f) {
    if ((float)(std::abs(x - y)) < eps) {
        return true;
    }
    return false;
}


glm::mat3 translateMatrix(const glm::vec3& dir) {
    glm::mat3 outputMat(1.0f);
    glm::vec3 axisZ(0.0f, 0.0f, 1.0f);
    if (isEqual(dir.x, 0.0f) && isEqual(dir.y, 0.0f)) {
        return outputMat;
    } else {
        auto axisA = glm::normalize(glm::cross(dir, axisZ));
        auto axisB = glm::normalize(glm::cross(axisA, dir));
        outputMat[0] = axisB;
        outputMat[1] = axisA;
        outputMat[2] = glm::normalize(dir);

        return outputMat;
    }
}

glm::mat3 makeRotateMatrixWithAxis(
    const glm::mat3& rotateM,
    const glm::vec3& axis)
{
    auto transM = translateMatrix(axis);
    return transM * rotateM * glm::inverse(transM);
}

class LSystem {
    std::map<char, std::string> rules;
    std::string initialString;
    std::string currentString;

public:
    LSystem() = default;

    void setInitialString(std::string& str) {
        initialString = str;
    }

    void addRule(char symbol, std::string& rule) {
        rules[symbol] = rule;
    }

    const std::string& getCurrentString() const {
        return currentString;
    }

    void buildSystem (size_t numIterations) {
        currentString = initialString;

        for (size_t i = 0; i < numIterations; ++i) {
            currentString = oneStep(currentString);
        }
    }

protected:
    std::string oneStep(const std::string& in) const {
        std::string output;
        for (size_t i = 0; i < in.length(); ++i) {
            auto it = rules.find(in[i]);
            if (it != rules.end()) {
                output += it->second;
            } else {
                output += in[i];
            }
        }
        return output;
    }
};


class TreeApplication : public Application, public LSystem{
    struct State {
        glm::vec3 pos;
        glm::vec3 dir;
        glm::vec3 angle;
        float length;
        float r1;
        float r2;
        float angleZ = 0.0f;
        bool invert;
        size_t level;

        State():
            dir(0.0f, 0.0f, 1.0f),
            angle(0.0f, 0.0f, 0.0f),
            invert(false),
            level(0) { }

        State(glm::vec3& angle,
              float length,
              float radius1,
              float radius2
        ):
            dir(0.0f, 0.0f, 1.0f),
            angle(angle),
            length(length),
            r1(radius1),
            r2(radius2) { }
    };

public:
    std::vector<MeshPtr> meshes;
    std::vector<MeshPtr> leaves;
    std::stack<State> states;
    MeshPtr marker;
    MeshPtr backgroundCube;
    MeshPtr ground;

    ShaderProgramPtr shader;
    ShaderProgramPtr leavesShader;
    ShaderProgramPtr markerShader;
    ShaderProgramPtr skyBoxShader;

    TexturePtr barkTexture;
    TexturePtr leaveTexture;
    TexturePtr skyBoxTexture;
    TexturePtr groundTexture;

    GLuint sampler;
    GLuint cubeTexSampler;
    GLuint groundSampler;
    LightInfo light;
    //Переменные для управления положением одного источника света
    float _lr = 3.0;
    float _phi = 0.0;
    float _theta = glm::pi<float>() * 0.25f;

    State curState;
    float distScale;
    float angleScale;
    size_t iterationsCount = 1;
    size_t details = 10;

    int maxAvailableAnisotropy;
    int maxAnisotropy = 4;

    TreeApplication(
        glm::vec3& angle, float length, float radius1, float radius2,
        float angleScale, size_t iterationsCount, size_t details
    ):
        curState(angle, length, radius1, radius2), distScale(radius2 / radius1),
        angleScale(angleScale), iterationsCount(iterationsCount), details(details)
    { }

    void setAngle(glm::vec3& newAngle) {
        curState.angle = newAngle;
    }

    void setLength(float newLength) {
        curState.length = newLength;
    }

    void setRadius(float r1, float r2) {
        curState.r1 = r1;
        curState.r2 = r2;
    }

    void setDistScale (float newScale) {
        distScale = newScale;
    }

    void setAngleScale (float scale) {
        angleScale = scale;
    }

    void setIterationsCount(float newIterationsCount) {
        iterationsCount = newIterationsCount;
    }

    void setDetails(float newDetails) {
        details = newDetails;
    }

    void addMesh() {
        float betaCos;
        float betaSin;
        float gammaSin;

        betaCos = curState.dir.z;
        betaSin = sqrt(1.0f - betaCos*betaCos);
        if (!isEqual(betaSin, 0.0f)) {
            gammaSin = curState.dir.x / (betaSin);
        } else {
            gammaSin = 0.0f;
        }

        float phi = (float)acos(betaCos);
        float theta = (float)asin(gammaSin);

        glm::mat4 Model = glm::translate(
                glm::mat4(1.0f),
                glm::vec3(curState.pos.x, curState.pos.y, curState.pos.z)
        );

        if (std::signbit(curState.dir.y)) {
            Model = glm::rotate(Model, theta, glm::vec3(0.0f, 0.0f, 1.0f));
            Model = glm::rotate(Model, phi, glm::vec3(1.0f, 0.0f, 0.0f));
        } else {
            Model = glm::rotate(Model, -theta, glm::vec3(0.0f, 0.0f, 1.0f));
            Model = glm::rotate(Model, -phi, glm::vec3(1.0f, 0.0f, 0.0f));
        }

        if (curState.level < iterationsCount) {
            float curr2 = curState.r2;
            if (curState.level == iterationsCount - 1) {
                curr2 = 0.0001f;
            }
            meshes.emplace_back(
                makeCylinder(curState.r1, curr2,
                             curState.length, details,
                             0.0, 0.0, 0.0)
            );
            meshes.back()->setModelMatrix(Model);
        } else {
            leaves.emplace_back(
                makeLeave(curState.r1, true)
            );
            leaves.back()->setModelMatrix(Model);
            leaves.emplace_back(
                makeLeave(curState.r1, false)
            );
            leaves.back()->setModelMatrix(Model);
        }

        curState.level++;
        curState.pos += curState.dir * curState.length;
        float curDistScale = curState.r2 / curState.r1;
        curState.r1 *= curDistScale;
        curState.r2 *= curDistScale * 0.9f;
        curState.length *= distScale;
        curState.angle *= angleScale;

    }

    void parseSymbol(char symbol) {
        if (symbol == 'F') {
            addMesh();
            return;
        }
        if (symbol == 'f') {
            curState.level++;
            curState.pos += curState.dir * curState.length;
            curState.r1 *= distScale;
            curState.r2 *= distScale;
            curState.length *= distScale;
            curState.angle *= angleScale;
            return;
        }
        if (symbol == '[') {
            states.push(curState);
            return;
        }
        if (symbol == ']') {
            curState = states.top();
            states.pop();
            return;
        }
        if (symbol == '+') {
            glm::mat3 rotateM;
            if (!curState.invert) {
                rotateM = rotateMatrix(curState.angle.x, 0.0f, 0.0f);
            } else {
                rotateM = rotateMatrix(-curState.angle.x, 0.0f, 0.0f);
            }
            auto rotateZ = rotateMatrix(0.0f, 0.0f, curState.angleZ);
            curState.angleZ = 0.0f;
            rotateM = rotateZ * rotateM;
            auto totalM = makeRotateMatrixWithAxis(rotateM, curState.dir);
            curState.dir = totalM * curState.dir;
            return;
        }
        if (symbol == '-') {
            glm::mat3 rotateM;
            if (!curState.invert) {
                rotateM = rotateMatrix(-curState.angle.x, 0.0f, 0.0f);
            } else {
                rotateM = rotateMatrix(curState.angle.x, 0.0f, 0.0f);
            }
            auto rotateZ = rotateMatrix(0.0f, 0.0f, curState.angleZ);
            curState.angleZ = 0.0f;
            rotateM = rotateZ * rotateM;
            auto totalM = makeRotateMatrixWithAxis(rotateM, curState.dir);
            curState.dir = totalM * curState.dir;
            return;
        }
        if (symbol == '<') {
            glm::mat3 rotateM;
            if (!curState.invert) {
                rotateM = rotateMatrix(0.0f, curState.angle.y, 0.0f);
            } else {
                rotateM = rotateMatrix(0.0f, -curState.angle.y, 0.0f);
            }
            auto rotateZ = rotateMatrix(0.0f, 0.0f, curState.angleZ);
            curState.angleZ = 0.0f;
            rotateM = rotateZ * rotateM;
            auto totalM = makeRotateMatrixWithAxis(rotateM, curState.dir);
            curState.dir = totalM * curState.dir;
            return;
        }
        if (symbol == '>') {
            glm::mat3 rotateM;
            if (!curState.invert) {
                rotateM = rotateMatrix(0.0f, -curState.angle.y, 0.0f);
            } else {
                rotateM = rotateMatrix(0.0f, curState.angle.y, 0.0f);
            }
            auto rotateZ = rotateMatrix(0.0f, 0.0f, curState.angleZ);
            curState.angleZ = 0.0f;
            rotateM = rotateZ * rotateM;
            auto totalM = makeRotateMatrixWithAxis(rotateM, curState.dir);
            curState.dir = totalM * curState.dir;
            return;
        }
        if (symbol == '&') {
//            glm::mat3 rotateM;
//            if (!curState.invert) {
//                rotateM = rotateMatrix(0.0f, 0.0f, curState.angle.z);
//            } else {
//                rotateM = rotateMatrix(0.0f, 0.0f, -curState.angle.z);
//            }
//            auto totalM = makeRotateMatrixWithAxis(rotateM, curState.dir);
//            curState.dir = totalM * curState.dir;
            if (!curState.invert) {
                curState.angleZ += curState.angle.z;
            } else {
                curState.angleZ -= curState.angle.z;
            }
            return;
        }
        if (symbol == '^') {
//            glm::mat3 rotateM;
//            if (!curState.invert) {
//                rotateM = rotateMatrix(0.0f, 0.0f, -curState.angle.z);
//            } else {
//                rotateM = rotateMatrix(0.0f, 0.0f, curState.angle.z);
//            }
//            auto totalM = makeRotateMatrixWithAxis(rotateM, curState.dir);
//            curState.dir = totalM * curState.dir;
            if (!curState.invert) {
                curState.angleZ -= curState.angle.z;
            } else {
                curState.angleZ += curState.angle.z;
            }
            return;
        }
        if (symbol == '!') {
            if (curState.invert) {
                curState.invert = false;
            } else {
                curState.invert = true;
            }

            return;
        }
    }

    void initGL() override {
        Application::initGL();
        glGetIntegerv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &maxAvailableAnisotropy);
        maxAnisotropy = std::min(maxAnisotropy, maxAvailableAnisotropy);
    }

    void initSamplers() {
        glGenSamplers(1, &sampler);
        glSamplerParameteri(sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);

        glGenSamplers(1, &cubeTexSampler);
        glSamplerParameteri(cubeTexSampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glSamplerParameteri(cubeTexSampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glSamplerParameteri(cubeTexSampler, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glSamplerParameteri(cubeTexSampler, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glSamplerParameteri(cubeTexSampler, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

        glGenSamplers(1, &groundSampler);
        glSamplerParameteri(groundSampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glSamplerParameteri(groundSampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glSamplerParameteri(groundSampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(groundSampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glSamplerParameterf(groundSampler, GL_TEXTURE_MAX_ANISOTROPY_EXT, maxAnisotropy);
    }

    void makeScene() override {
        Application::makeScene();

        buildSystem(iterationsCount);

        std::string result = getCurrentString();
        for (const auto& symbol: result) {
            if (symbol == 'A') {
                parseSymbol('F');
            } else {
                parseSymbol(symbol);
            }
        }

        ground = makeGroundPlane(50.0f, 50.0f);
        marker = makeSphere(0.1f);
        backgroundCube = makeCube(10.0f);

        shader = std::make_shared<ShaderProgram>(
            "499DerevyankoData/texture.vert",
            "499DerevyankoData/texture.frag"
        );

        leavesShader = std::make_shared<ShaderProgram>(
            "499DerevyankoData/texture.vert",
            "499DerevyankoData/leaves.frag"
        );

        markerShader = std::make_shared<ShaderProgram>(
            "499DerevyankoData/marker.vert",
            "499DerevyankoData/marker.frag"
        );

        skyBoxShader = std::make_shared<ShaderProgram>(
            "499DerevyankoData/skybox.vert",
            "499DerevyankoData/skybox.frag"
        );

        //Инициализация значений переменных освщения
        light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        light.ambient = glm::vec3(0.2, 0.2, 0.2);
        light.diffuse = glm::vec3(0.8, 0.8, 0.8);
        light.specular = glm::vec3(1.0, 1.0, 1.0);

        barkTexture = loadTexture("499DerevyankoData/TexturesCom_BarkDecidious0132_M.jpg");
        leaveTexture = loadTexture("499DerevyankoData/TexturesCom_Leaves0166_1_alphamasked_S.png");
        skyBoxTexture = loadCubeTexture("499DerevyankoData/skycube");
        groundTexture = loadTexture("499DerevyankoData/TexturesCom_Grass0131_1_seamless_S.jpg");

        // Инициализация всех сэмплеров
        initSamplers();
    }

    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

            if (ImGui::CollapsingHeader("Light"))
            {
                ImGui::ColorEdit3("ambient", glm::value_ptr(light.ambient));
                ImGui::ColorEdit3("diffuse", glm::value_ptr(light.diffuse));
                ImGui::ColorEdit3("specular", glm::value_ptr(light.specular));

                ImGui::SliderFloat("radius", &_lr, 0.1f, 10.0f);
                ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
                ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());
            }
        }
        ImGui::End();
    }

    void draw() override {
        Application::draw();

        int width, height;

        //Получаем текущие размеры экрана и выставлям вьюпорт
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Рисуем SkyBox
        {
            skyBoxShader->use();

            glm::vec3 cameraPos = glm::vec3(glm::inverse(_camera.viewMatrix)[3]);

            skyBoxShader->setVec3Uniform("cameraPos", cameraPos);
            skyBoxShader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
            skyBoxShader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

            //Для преобразования координат в текстурные координаты нужна специальная матрица
            glm::mat3 textureMatrix = glm::mat3(0.0f, 0.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
            skyBoxShader->setMat3Uniform("textureMatrix", textureMatrix);

            glActiveTexture(GL_TEXTURE0);
            glBindSampler(0, cubeTexSampler);
            skyBoxTexture->bind();
            skyBoxShader->setIntUniform("cubeTex", 0);

            glDepthMask(GL_FALSE); //Отключаем запись в буфер глубины

            backgroundCube->draw();

            glDepthMask(GL_TRUE); //Включаем обратно запись в буфер глубины
        }

        // рисуем землю, и дерево
        shader->use();

        //Загружаем на видеокарту значения юниформ-переменные: время и матрицы
        shader->setFloatUniform("time", (float)glfwGetTime()); //передаем время в шейдер

        shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(light.position, 1.0));

        shader->setVec3Uniform("light.pos", lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
        shader->setVec3Uniform("light.La", light.ambient);
        shader->setVec3Uniform("light.Ld", light.diffuse);
        shader->setVec3Uniform("light.Ls", light.specular);

        glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
        glBindSampler(0, groundSampler);
        groundTexture->bind();
        shader->setIntUniform("diffuseTex", 0);

        {
            shader->setMat4Uniform("modelMatrix", ground->modelMatrix());
            shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * ground->modelMatrix()))));

            ground->draw();
        }

        glBindSampler(0, sampler);

        barkTexture->bind();

        //Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
        for (size_t i = 0; i < meshes.size(); ++i) {
            shader->setMat4Uniform("modelMatrix", meshes[i]->modelMatrix());
            shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * meshes[i]->modelMatrix()))));

            meshes[i]->draw();
        }

        leavesShader->use();

        leavesShader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        leavesShader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        leavesShader->setVec3Uniform("light.pos", lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
        leavesShader->setVec3Uniform("light.La", light.ambient);
        leavesShader->setVec3Uniform("light.Ld", light.diffuse);
        leavesShader->setVec3Uniform("light.Ls", light.specular);
        leavesShader->setIntUniform("diffuseTex", 0);

        leaveTexture->bind();

        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glEnable(GL_BLEND);
        glEnable(GL_CULL_FACE);


        //Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
        for (size_t i = 0; i < leaves.size(); ++i) {
            leavesShader->setMat4Uniform("modelMatrix", leaves[i]->modelMatrix());
            leavesShader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * leaves[i]->modelMatrix()))));

            leaves[i]->draw();
        }

        glDisable(GL_CULL_FACE);
        glDisable(GL_BLEND);

        //Рисуем маркеры для всех источников света
        {
            markerShader->use();

            markerShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), light.position));
            markerShader->setVec4Uniform("color", glm::vec4(light.diffuse, 1.0f));

            marker->draw();
        }

        glBindSampler(0, 0);
        glUseProgram(0);
    }
};


int main()
{
    std::string initString("A");
    std::string rule("F[-A][<A][+A][>A]&[A]");

    glm::vec3 angle(
        (float)glm::pi<float>() * 0.45f,
        (float)glm::pi<float>() * 0.45f,
        (float)glm::pi<float>() * 0.25f
    );
    float length = 0.5f;
    float radius1 = 0.12f;
    float radius2 = 0.07f;
    float angleScale = 0.85f;
    size_t iterationsCount = 5;
    size_t details = 10;

    TreeApplication app(angle, length, radius1, radius2,
                        angleScale, iterationsCount, details);

    app.setInitialString(initString);
    app.addRule('A', rule);

    app.start();

    return 0;
}
