#include "Main.h"

#include <stack>
#include <iostream>


void printMat3(glm::mat3& mat) {
    std::cout << "x: " << mat[0].x << " x: " << mat[1].x << " x: " << mat[2].x << std::endl;
    std::cout << "y: " << mat[0].y << " y: " << mat[1].y << " y: " << mat[2].y << std::endl;
    std::cout << "z: " << mat[0].z << " z: " << mat[1].z << " z: " << mat[2].z << std::endl;
}


bool isEqual(float x, float y, float eps = 0.0001f) {
    if ((float)(std::abs(x - y)) < eps) {
        return true;
    }
    return false;
}


class LSystem {
    std::map<char, std::string> rules;
    std::string initialString;
    std::string currentString;

public:
    LSystem() = default;

    void setInitialString(std::string& str) {
        initialString = str;
    }

    void addRule(char symbol, std::string& rule) {
        rules[symbol] = rule;
    }

    const std::string& getCurrentString() const {
        return currentString;
    }

    void buildSystem (size_t numIterations) {
        currentString = initialString;

        for (size_t i = 0; i < numIterations; ++i) {
            currentString = oneStep(currentString);
        }
    }

protected:
    std::string oneStep(const std::string& in) const {
        std::string output;
        for (size_t i = 0; i < in.length(); ++i) {
            auto it = rules.find(in[i]);
            if (it != rules.end()) {
                output += it->second;
            } else {
                output += in[i];
            }
        }
        return output;
    }
};


class TreeApplication : public Application, public LSystem{
    struct State {
        glm::vec3 pos;
        glm::vec3 dir;
        float angle;
        float length;
        float r1;
        float r2;
        bool invert;
        size_t level;

        State(): dir(0.0f, 0.0f, 1.0f), angle(0.0), invert(false), level(0) { }

        State(float angle, float length, float radius1, float radius2):
            dir(0.0f, 0.0f, 1.0f), angle(angle), length(length), r1(radius1), r2(radius2) { }
    };

public:
    std::vector<MeshPtr> meshes;
    std::stack<State> states;

    ShaderProgramPtr shader;
    State curState;
    float distScale;
    float angleScale;
    size_t iterationsCount = 1;
    size_t details = 10;

    TreeApplication(
        float angle, float length, float radius1, float radius2,
        float angleScale, size_t iterationsCount, size_t details
    ):
        curState(angle, length, radius1, radius2), distScale(radius2 / radius1),
        angleScale(angleScale), iterationsCount(iterationsCount), details(details)
    { }

    void setAngle(float newAngle) {
        curState.angle = newAngle;
    }

    void setLength(float newLength) {
        curState.length = newLength;
    }

    void setRadius(float r1, float r2) {
        curState.r1 = r1;
        curState.r2 = r2;
    }

    void setDistScale (float newScale) {
        distScale = newScale;
    }

    void setAngleScale (float scale) {
        angleScale = scale;
    }

    void setIterationsCount(float newIterationsCount) {
        iterationsCount = newIterationsCount;
    }

    void setDetails(float newDetails) {
        details = newDetails;
    }

    void addMesh() {
        float betaCos;
        float betaSin;
        float gammaSin;

        betaCos = curState.dir.z;
        betaSin = sqrt(1.0f - betaCos*betaCos);
        if (!isEqual(betaSin, 0.0f)) {
            gammaSin = curState.dir.x / (betaSin);
        } else {
            gammaSin = 0.0f;
        }

        float phi = (float)acos(betaCos);
        float theta = (float)asin(gammaSin);

        glm::mat4 Model =
            glm::translate(glm::mat4(1.0f), glm::vec3(curState.pos.x, curState.pos.y, curState.pos.z));

        if (std::signbit(curState.dir.y)) {
            Model = glm::rotate(Model, theta, glm::vec3(0.0f, 0.0f, 1.0f));
            Model = glm::rotate(Model, phi, glm::vec3(1.0f, 0.0f, 0.0f));
        } else {
            Model = glm::rotate(Model, -theta, glm::vec3(0.0f, 0.0f, 1.0f));
            Model = glm::rotate(Model, -phi, glm::vec3(1.0f, 0.0f, 0.0f));
        }

        meshes.emplace_back(makeCylinder(curState.r1, curState.r2, curState.length, details, 0.0, 0.0, 0.0));
        meshes.back()->setModelMatrix(Model);

        curState.level++;
        curState.pos += curState.dir * curState.length;
        curState.r1 *= distScale;
        curState.r2 *= distScale;
        curState.length *= distScale;
        curState.angle *= angleScale;

    }

    void parseSymbol(char symbol) {
        if (symbol == 'F') {
            addMesh();
            return;
        }
        if (symbol == 'f') {
            return;
        }
        if (symbol == '[') {
            states.push(curState);
            return;
        }
        if (symbol == ']') {
            curState = states.top();
            states.pop();
            return;
        }
        if (symbol == '+') {
            glm::mat3 rotateM;
            if (!curState.invert) {
                rotateM = rotateMatrix(curState.angle, 0.0f, 0.0f);
            } else {
                rotateM = rotateMatrix(-curState.angle, 0.0f, 0.0f);
            }
            curState.dir = rotateM * curState.dir;
            return;
        }
        if (symbol == '-') {
            glm::mat3 rotateM;
            if (!curState.invert) {
                rotateM = rotateMatrix(-curState.angle, 0.0f, 0.0f);
            } else {
                rotateM = rotateMatrix(curState.angle, 0.0f, 0.0f);
            }
            curState.dir = rotateM * curState.dir;
            return;
        }
        if (symbol == '<') {
            glm::mat3 rotateM;
            if (!curState.invert) {
                rotateM = rotateMatrix(0.0f, curState.angle, 0.0f);
            } else {
                rotateM = rotateMatrix(0.0f, -curState.angle, 0.0f);
            }
            curState.dir = rotateM * curState.dir;
            return;
        }
        if (symbol == '>') {
            glm::mat3 rotateM;
            if (!curState.invert) {
                rotateM = rotateMatrix(0.0f, -curState.angle, 0.0f);
            } else {
                rotateM = rotateMatrix(0.0f, curState.angle, 0.0f);
            }
            curState.dir = rotateM * curState.dir;
            return;
        }
        if (symbol == '&') {
            glm::mat3 rotateM;
            if (!curState.invert) {
                rotateM = rotateMatrix(0.0f, 0.0f, curState.angle);
            } else {
                rotateM = rotateMatrix(0.0f, 0.0f, -curState.angle);
            }
            curState.dir = rotateM * curState.dir;
            return;
        }
        if (symbol == '^') {
            glm::mat3 rotateM;
            if (!curState.invert) {
                rotateM = rotateMatrix(0.0f, 0.0f, -curState.angle);
            } else {
                rotateM = rotateMatrix(0.0f, 0.0f, curState.angle);
            }
            curState.dir = rotateM * curState.dir;
            return;
        }
        if (symbol == '!') {
            if (curState.invert) {
                curState.invert = false;
            } else {
                curState.invert = true;
            }

            return;
        }
    }

    void makeScene() override {
        Application::makeScene();

        buildSystem(iterationsCount);

        std::string result = getCurrentString();
        for (const auto& symbol: result) {
            parseSymbol(symbol);
        }

        shader = std::make_shared<ShaderProgram>("shaders/shader.vert", "shaders/shader.frag");

    }

    void draw() override {
        Application::draw();

        int width, height;

        //Получаем текущие размеры экрана и выставлям вьюпорт
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Подключаем шейдер
        shader->use();

        //Загружаем на видеокарту значения юниформ-переменные: время и матрицы
        shader->setFloatUniform("time", (float)glfwGetTime()); //передаем время в шейдер

        shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        //Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
        for (size_t i = 0; i < meshes.size(); ++i) {
            shader->setMat4Uniform("modelMatrix", meshes[i]->modelMatrix());
            meshes[i]->draw();
        }
    }
};


int main()
{
    std::string initString("F");
    std::string rule("F[-F]<[F]>[+F][F]");

    float angle = (float)glm::pi<float>() * 0.35f;
    float length = 0.4f;
    float radius1 = 0.12f;
    float radius2 = 0.08f;
    float angleScale = 0.85f;
    size_t iterationsCount = 6;
    size_t details = 10;

    TreeApplication app(angle, length, radius1, radius2,
                        angleScale, iterationsCount, details);

    app.setInitialString(initString);
    app.addRule('F', rule);

    app.start();

    return 0;
}
